
import Foundation

enum NoteType: String {
    case Simple = "SimpleNotes"
    case AppleBuilty = "AppleBuilties"
}

class NoteModel: Identifiable {
    
    var title: String = ""
    var detail: String = ""
    var creationDate: String = ""
    var modificationDate: String = ""
    var type:NoteType = .Simple
    var noteDateToPerform:String = ""
    var isReminderOn: Bool = true
    
    
    var varities:[AppleVaritySizeModel] = []
    var varityName: String = "Select varity"
    var currentVarity =  AppleVaritySizeModel()
    
    
    var senderName: String = ""
    var consigneeName: String = ""
    var truckNumber: String = ""
    var ownersName: String = ""
    var prop: String = ""
    var fromLocation: String = ""
    var toLocation: String = ""
    var freight: String = ""
    var builtyDateString: String = ""
    var builtyDate: Date = Date()
    
    init() {
    }
    
    var dictionary: [String: Any] {
        if type == .Simple{
            return ["title": title,
            "detail": detail,
            "creationDate": creationDate,
            "modificationDate": modificationDate,
            "type": type.rawValue,
            "noteDateToPerform": noteDateToPerform,"isReminderOn": isReminderOn]
        }else{
            var varitiesDictArray: [[String:Any]] = [[:]]
            for varity in varities{
                varitiesDictArray.append(varity.dictionary)
            }
            return ["title": title,
            "detail": detail,
            "creationDate": creationDate,
            "modificationDate": modificationDate,
            "type": type.rawValue,
            "noteDateToPerform": noteDateToPerform,"isReminderOn": isReminderOn, "varities": varitiesDictArray]
        }
        
    }
    
}

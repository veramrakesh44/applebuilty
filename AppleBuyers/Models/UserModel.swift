

import UIKit

enum UserType:String {
    case grover = "grover"
    case nonGrover = "nonGrover"
}

struct UserModelKeys {
    static let isLoggedIn = "isLoggedIn"
    static let name = "name"
    static let uid = "uid"
    static let phoneNumber = "phoneNumber"
    static let userType = "userType"
    static let authVerificationID = "authVerificationID"
}

class UserDB {
    static let shared = UserDB()
    var user = UserModel()
    private init(){
        user = UserModel()
        self.loadUser()
    }
    
    func loadUser(){
        user.name = UserDefaults.standard.value(forKey: UserModelKeys.name) as? String ?? ""
        user.uid = UserDefaults.standard.value(forKey: UserModelKeys.uid) as? String ?? ""
        user.phoneNumber = UserDefaults.standard.value(forKey: UserModelKeys.phoneNumber) as? String ?? ""
        let userTypeString = UserDefaults.standard.value(forKey: UserModelKeys.userType) as? String ?? ""
        if userTypeString == UserType.nonGrover.rawValue{
            user.userType = .nonGrover
        }else{
            user.userType = .grover
        }
    }
    
    func saveAuthID(verificationID: String){
        UserDefaults.standard.set(verificationID, forKey: UserModelKeys.authVerificationID)
        UserDefaults.standard.synchronize()
    }
    
    func verificationID() -> String?{
        return UserDefaults.standard.value(forKey: UserModelKeys.authVerificationID) as? String
    }
    
    func saveUser(user: UserModel){
        UserDefaults.standard.set(user.name, forKey: UserModelKeys.name)
        UserDefaults.standard.set(user.uid, forKey: UserModelKeys.uid)
        UserDefaults.standard.set(user.phoneNumber, forKey: UserModelKeys.phoneNumber)
        UserDefaults.standard.set(user.userType.rawValue, forKey: UserModelKeys.userType)
        UserDefaults.standard.synchronize()
        loadUser()
    }
    
    func deleteUser(){
        UserDefaults.standard.set(nil, forKey: UserModelKeys.name)
        UserDefaults.standard.set(nil, forKey: UserModelKeys.uid)
        UserDefaults.standard.set(nil, forKey: UserModelKeys.phoneNumber)
        UserDefaults.standard.set(nil, forKey: UserModelKeys.userType)
        UserDefaults.standard.setValue(nil, forKey: UserModelKeys.isLoggedIn)
        UserDefaults.standard.synchronize()
    }
    
    var isLoggedIn: Bool {
        set{
            loggedIn()
        }
        get{
            getIsloggedIn()
        }
    }
    
    
    func getIsloggedIn() -> Bool{
        if let isLoggedIn = UserDefaults.standard.value(forKey:  UserModelKeys.isLoggedIn) as? Bool, isLoggedIn{
            return true
        }else{
            return false
        }
    }
    
    func loggedIn(){
        UserDefaults.standard.setValue(true, forKey: UserModelKeys.isLoggedIn)
        UserDefaults.standard.synchronize()
    }
}

class UserModel: Identifiable {
    
    var name: String = ""
    var gender: String = ""
    var uid: String = ""
    var email: String = ""
    var userType: UserType = .nonGrover
    var phoneNumber: String = ""

    init() {
    }
    
    var dictionary: [String: Any] {
        return ["name": name,
                "gender": gender,
                "uid": uid,
                "email": email,
                "userType":userType.rawValue,
                "phoneNumber":phoneNumber]
    }
    
    
}



import Foundation
class AppleVaritySizeModel: Identifiable {
    
    var extraLarge: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var large: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var medium: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var small: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var extraSmall: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var sevenLyrs: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var twoFortyD: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var pittu: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var mix: String = ""{
        didSet{
            self.calculateTotal()
        }
    }
    var total: String = "0"
    var varityName: String = "Select Varity"
    var lotNumber = ""
   
    func calculateTotal(){
        let extraLarge = Int(self.extraLarge) ?? 0
        let large = Int(self.large) ?? 0
        let small = Int(self.small) ?? 0
        let extraSmall = Int(self.extraSmall) ?? 0
        let medium = Int(self.medium) ?? 0
        let pittu = Int(self.pittu) ?? 0
        let mix = Int(self.mix) ?? 0
        let sevenLyrs = Int(self.sevenLyrs) ?? 0
        let twoFortyD = Int(self.twoFortyD) ?? 0
        
        self.total = "\(extraLarge + large + small + extraSmall + medium + pittu + mix + sevenLyrs + twoFortyD)"
    }
    
 
    
    var dictionary: [String: Any] {
        return ["large": large,
                "small": small,
                "medium": medium,
                "extraSmall": extraSmall,
                "sevenLyrs": sevenLyrs,
                "twoFortyD": twoFortyD,
                "pittu": pittu,
                "mix": mix,
                "total":total,
                "varityName":varityName,
                "lotNumber": lotNumber]
    }
    
}

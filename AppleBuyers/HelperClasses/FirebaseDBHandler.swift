

import UIKit
import Firebase
import FirebaseFirestore


class FirebaseDBHandler: NSObject {
    let db = Firestore.firestore()
    func saveUserData(user: UserModel, completion: @escaping (_ isSuccess: Bool, _ error: Error?) -> Void){
       
        db.collection("Users").document(user.uid).setData(user.dictionary, completion: { (error) in
            guard let error =  error else{
                  completion(true, nil)
                  return
              }
            completion(false, error)
        })
    }

    func getUsers(completion: @escaping (_ user: [UserModel], _ error: Error?) -> Void){
        db.collection("Users").getDocuments { (snap, error) in
            if let error = error{
                completion([], error)
            }else{
                var users: [UserModel] = []
                for document in snap!.documents{
                   let data =  document.data()
                  //  users.append(UserModel(name: "", email: data["email"] as? String ?? "", gender: data["gender"] as? String ?? "", uid: data["uid"] as? String ?? ""))
                }
                completion(users, nil)
            }
        }
    }
    
    func getNotes(noteType: NoteType, completion: @escaping (_ notes: [NoteModel], _ error: Error?) -> Void){
        
        db.collection(noteType.rawValue).document(UserDB.shared.user.uid).collection("details").getDocuments { (snap, error) in
            if let error = error{
                completion([], error)
            }else{
                var notes: [NoteModel] = []
                for document in snap!.documents{
                    let data =  document.data()
                    let note = NoteModel()
                    note.title = data["title"] as? String ?? ""
                    note.detail = data["detail"] as? String ?? ""
                    note.creationDate = data["creationDate"] as? String ?? ""
                    note.modificationDate = data["modificationDate"] as? String ?? ""
                    note.noteDateToPerform = data["noteDateToPerform"] as? String ?? ""
                    
                    notes.append(note)
                }
                completion(notes, nil)
            }
        }
    }

    
    func saveNote(noteType: NoteType ,note: NoteModel, completion: @escaping (_ isSuccess: Bool, _ error: Error?) -> Void){
       
        db.collection(noteType.rawValue).document(UserDB.shared.user.uid).collection("details").addDocument(data: note.dictionary) { (error) in
            guard let error =  error else{
                completion(true, nil)
                return
            }
            completion(false, error)
        }
    }
    
    func sendOTP(phoneNumber: String, completion: @escaping (_ error: Error?) -> Void){
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
          if let error = error {
            completion(error)
            return
          }
            UserDB.shared.saveAuthID(verificationID: verificationID ?? "")
            completion(nil)
        }
    }
    
    func signInWithPhonenumber(verificationCode: String, completion: @escaping (_ error: Error?) -> Void){
        guard let verificationID = UserDB.shared.verificationID() else {
            return
        }
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if let error = error{
                completion(error)
            }else{
                let user = UserDB.shared.user
                user.uid = authResult?.user.uid ?? ""
                user.phoneNumber = authResult?.user.phoneNumber ?? ""
                UserDB.shared.saveUser(user: user)
                completion(nil)
            }
        }
    }
    
    func logout(){
        do{
          try Auth.auth().signOut()
            
        }catch{}
        
    }
}


import Foundation

enum DateCompare {
    case previous
    case now
    case future
}

extension Date {
    static func today() -> Date {
        return Date()
    }
    
    func next(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.next,
                   weekday,
                   considerToday: considerToday)
    }
    
    func previous(_ weekday: Weekday, considerToday: Bool = false) -> Date {
        return get(.previous,
                   weekday,
                   considerToday: considerToday)
    }
    
    func get(_ direction: SearchDirection,
             _ weekDay: Weekday,
             considerToday consider: Bool = false) -> Date {
        
        let dayName = weekDay.rawValue
        
        let weekdaysName = getWeekDaysInEnglish().map { $0.lowercased() }
        
        assert(weekdaysName.contains(dayName), "weekday symbol should be in form \(weekdaysName)")
        
        let searchWeekdayIndex = weekdaysName.firstIndex(of: dayName)! + 1
        
        let calendar = Calendar(identifier: .gregorian)
        
        if consider && calendar.component(.weekday, from: self) == searchWeekdayIndex {
            return self
        }
        
        var nextDateComponent = calendar.dateComponents([.hour, .minute, .second], from: self)
        nextDateComponent.weekday = searchWeekdayIndex
        
        let date = calendar.nextDate(after: self,
                                     matching: nextDateComponent,
                                     matchingPolicy: .nextTime,
                                     direction: direction.calendarSearchDirection)
        
        return date!
    }
    
    func setTime(timeZoneAbbrev: String = "UTC") -> Date? {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let cal = Calendar.current
        var components = cal.dateComponents(x, from: self)
        
        components.timeZone = TimeZone(abbreviation: timeZoneAbbrev)
        components.hour = getHourMinuteSecond(date: Date()).0
        components.minute = getHourMinuteSecond(date: Date()).1
        components.second = getHourMinuteSecond(date: Date()).2
        
        return cal.date(from: components)
    }
    
    func getWeekDaysInEnglish() -> [String] {
        var calendar = Calendar(identifier: .gregorian)
        calendar.locale = Locale(identifier: "en_US_POSIX")
        return calendar.weekdaySymbols
    }
    
    enum Weekday: String {
        case monday, tuesday, wednesday, thursday, friday, saturday, sunday
    }
    
    enum SearchDirection {
        case next
        case previous
        
        var calendarSearchDirection: Calendar.SearchDirection {
            switch self {
            case .next:
                return .forward
            case .previous:
                return .backward
            }
        }
    }
    
    var customID: String{
        return String(self.timeIntervalSince1970).replacingOccurrences(of: ".", with: "")
    }
    
    func combineDateAndTime(date: Date, time: Date) -> Date {
        
        let calendar = Calendar.current
        
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date)
        let timeComponents = calendar.dateComponents([.hour, .minute, .second], from: time)
        
        var components = DateComponents()
        components.year = dateComponents.year
        components.month = dateComponents.month
        components.day = dateComponents.day
        components.hour = timeComponents.hour
        components.minute = timeComponents.minute
        components.second = timeComponents.second
        
        return calendar.date(from: components)!
    }
    
    func toString() -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"
        return dateFormatter.string(from: self)
    }
    
    func dayStartingDateTime() -> Date{
        let calendar = Calendar.current
        let startTime = calendar.startOfDay(for: self)
        return startTime
    }
    
    func monthRange() -> (Date, Date) {
        let calendar = Calendar.current
        var components = calendar.dateComponents([.era,.month,.year], from: self)
        
        components.day = 1
        
        let startDate = calendar.date(from: components)!
        let endDate   = calendar.date(byAdding: .month, value: 1, to: startDate)!
        
        return (startDate, endDate)
    }
    
    func dayEndDateTime() -> Date{
        let calendar = Calendar.current
        let endTime = calendar.date(bySettingHour: 23, minute: 59, second: 59, of: self)
        return endTime!
    }
    
    func previousDate() -> Date{
        let calendar = Calendar.current
        let previouseDate = calendar.date(byAdding: .day, value: -1, to: self)
        return previouseDate!
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int{
        let calendar = Calendar.current

        let components = calendar.dateComponents([.day], from: startDate, to: endDate)

        return components.day ?? 0
    }
    func nextDate() -> Date{
        let calendar = Calendar.current
        let nextDate = calendar.date(byAdding: .day, value: 1, to: self)
        return nextDate!
    }
    
    func nextDateAddingDays(days: Int) -> Date{
        let calendar = Calendar.current
        let nextDate = calendar.date(byAdding: .day, value: days, to: self)
        return nextDate!
    }
    
    func nextDateReducingDays(days: Int) -> Date{
        let calendar = Calendar.current
        let nextDate = calendar.date(byAdding: .day, value:  -days, to: self)
        return nextDate!
    }
    
    var time : String{
        let calendar = Calendar.current
        
        let hour = calendar.component(.hour, from: self)
        let minutes = calendar.component(.minute, from: self)
        //let seconds = calendar.component(.second, from: self)
        //print("hours = \(hour):\(minutes)")
        return "\(hour):\(minutes)"
    }
    
    func currentTimeMillis() -> String {
        return String(Int64(self.timeIntervalSince1970 * 1000))
    }
    
    var  timeInAMPM: String{
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm a"
        return formatter.string(from: self)
    }
    
    var fullDate:String{
        let formatter = DateFormatter()
        formatter.dateFormat = "MM-dd-YYYY"
        return formatter.string(from: self)
    }
    
    var month: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM"
        return dateFormatter.string(from: self)
    }
    
    var year: String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY"
        return dateFormatter.string(from: self)
    }
    
    var monthYearString: String{
        return ("\(month)\(", ")\(year)")
    }
    
    var  weakDay: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let weekDay = dateFormatter.string(from: self)
        return weekDay
    }
    
    var  weakDayShort: String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        let weekDay = dateFormatter.string(from: self)
        return weekDay
    }
    
    var dayNumber : String{
        let dateComponent = Calendar.current.dateComponents([.day], from: self)
        let day = dateComponent.day
        return String(day!)
    }
    func getString(with format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone.current // TimeZone(abbreviation:"UTC")!
        dateFormatter.dateFormat = format
        let strDate = dateFormatter.string(from: self)
        return strDate
    }
    
    func getThisMonthStart() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month, .day], from: self) as NSDateComponents
        //components.day += 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    func getCurrentYear() -> Date? {
        return Calendar.current.date(byAdding: .second, value: 1, to: self)
    }
    
    func getEndYear() -> Date? {
        return Calendar.current.date(byAdding: .year, value: 200, to: self)
    }
    func getHourMinuteSecond(date: Date) -> (Int, Int, Int){
        // *** create calendar object ***
        var calendar = NSCalendar.current

        // *** Get components using current Local & Timezone ***
        print(calendar.dateComponents([.year, .month, .day, .hour, .minute], from: date as Date))

        // *** define calendar components to use as well Timezone to UTC ***
        let unitFlags = Set<Calendar.Component>([.hour, .year, .minute])
        calendar.timeZone = TimeZone(identifier: "UTC")!

        // *** Get All components from date ***
        let components = calendar.dateComponents(unitFlags, from: date)
        print("All Components : \(components)")

        // *** Get Individual components from date ***
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)
        return (hour,minutes,seconds)
    }
    /*
     Other Methods
     */
    
   /*
     
    mutating func addDays(_ days: Int) {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        self = calendar.date(byAdding: .day, value: days, to: self)!
    }
    
    mutating func addHours(_ hours:Int){
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        self = calendar.date(byAdding: .hour, value: hours, to: self)!
    }
    
    mutating func addMonths(_ months: Int) {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        var dateComponents = DateComponents()
        dateComponents.month = months
        self = calendar.date(byAdding: dateComponents, to: self)!
    }
    
  
     func daysCount() -> Int {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        let dateComponents = DateComponents(year: calendar.component(.year, from: self), month: calendar.component(.month, from: self))
        let date = calendar.date(from: dateComponents)!
        let range = calendar.range(of: .day, in: .month, for: date)!
        let numDays = range.count
        
        return numDays
    }
    
    func dayName() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EE"
        return dateFormatter.string(from: self)
    }
    
    func startOfYear() -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        return calendar.date(from: Calendar.current.dateComponents([.year], from: calendar.startOfDay(for: self)))!
    }
    
    func startOfMonth() -> Date {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        let components = calendar.dateComponents([.year, .month], from: self)
        return calendar.date(from: components)!
    }
    
    func endOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
    
    private func compareValues(_ selfValue: Int, another: Int) -> DateCompare {
        if selfValue == another {
            return .now
        } else if selfValue < another {
            return .previous
        } else {
            return .future
        }
    }
    
   private func isNowYear() -> DateCompare {
        let calendar = Calendar.current
        let nowYear = calendar.component(.year, from: self)
        let anotherYear = calendar.component(.year, from: Date())
        return compareValues(nowYear, another: anotherYear)
    }
    
   private func isNowMonth() -> DateCompare {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        let nowMonth = calendar.component(.month, from: self)
        let anotherMonth = calendar.component(.month, from: Date())
        
        if isNowYear() == .previous {
            return .previous
        } else if isNowYear() == .future {
            return .future
        } else {
            return compareValues(nowMonth, another: anotherMonth)
        }
    }
    
    func isNowDay() -> DateCompare {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        let nowDay = calendar.component(.day, from: self)
        let anotherDay = calendar.component(.day, from: Date())
        
        if isNowYear() == .previous {
            return .previous
        } else if isNowYear() == .future {
            return .future
        } else {
            if isNowMonth() == .previous {
                return .previous
            } else if isNowMonth() == .future {
                return .future
            } else {
                return compareValues(nowDay, another: anotherDay)
            }
        }
    }
 
    
    
    func getDayAndMonth() -> (day: Int,month: Int) {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone.current //TimeZone(abbreviation: "UTC")!
        let month = calendar.component(.month, from: self)
        let day = calendar.component(.day, from: self)
        return (day, month)
        
    }
    
    var startOfWeek: Date {
        let date = Calendar.current.date(from: Calendar.current.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        let dslTimeOffset = NSTimeZone.local.daylightSavingTimeOffset(for: date)
        var returnDate = date.addingTimeInterval(dslTimeOffset)
        returnDate.addDays(1)
        return returnDate
    }
    
    var endOfWeek: Date {
        return Calendar.current.date(byAdding: .second, value: 604799, to: self.startOfWeek)!
    }
    
     
     
    func getLast10Year() -> Date? {
        return Calendar.current.date(byAdding: .year, value: -10, to: self)
    }
    
    
    func getLast6Month() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -6, to: self)
    }
    
    func getLast3Month() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -3, to: self)
    }
    
    func getYesterday() -> Date? {
        return Calendar.current.date(byAdding: .day, value: -1, to: self)
    }
    
    func getLast7Day() -> Date? {
        return Calendar.current.date(byAdding: .day, value: -7, to: self)
    }
    func getLast30Day() -> Date? {
        return Calendar.current.date(byAdding: .day, value: -30, to: self)
    }
    
    func getPreviousMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)
    }
 
    
     func getThisMonthEnd() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month += 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    
    //Last Month Start
    func getLastMonthStart() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    //Last Month End
    func getLastMonthEnd() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.day = 1
        components.day -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
     
    */
    
}

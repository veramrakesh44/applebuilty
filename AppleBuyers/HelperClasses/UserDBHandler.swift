
import Foundation

struct UserKeys {
    static let isLoggedIn = "isLoggedIn"
}

class UserDBHandler {
    
    func saveValue(key: String, value: Any){
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }
    
    func isLoggedIn() -> Bool{
        if let isLoggedIn = UserDefaults.standard.value(forKey: UserKeys.isLoggedIn) as? Bool, isLoggedIn{
            return true
        }
        return false
    }
    
}

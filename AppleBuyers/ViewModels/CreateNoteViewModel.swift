
import Foundation
import SwiftUI

class CreateNoteViewModel: ObservableObject{
  
    @Published var noteDateToPerform: Date = Date()
    @Published var calculatedHeight: CGFloat = 400
    @Published var message: String = ""
    @Published var isProcessCompleted: Bool = false
    @Published var isLoading: Bool = false
    @Published var noteModel = NoteModel()
    
    
    func saveSimpleNote(){
        if noteModel.title.isEmpty{
            self.isLoading.toggle()
            self.showAlert(message: "Enter Note Title")
        }else if noteModel.title.isEmpty{
            self.isLoading.toggle()
          self.showAlert(message: "Enter Note Description")
        }else{
            isLoading.toggle()
            noteModel.creationDate = Date().fullDate
            noteModel.modificationDate = Date().fullDate
            noteModel.noteDateToPerform = Date().fullDate
            FirebaseDBHandler().saveNote(noteType: .Simple, note: noteModel) { (isSuccess, error) in

                guard let error =  error else{
                    self.showAlert(message: "Note Saved Successfully")
                    return
                }
                self.showAlert(message: error.localizedDescription)
            }
        }
    }
    
    func showAlert(message: String){
        DispatchQueue.main.async {
            withAnimation(.spring()) {
                self.isLoading.toggle()
                self.isProcessCompleted.toggle()
                self.message = message
            }
           
        }
    }
}

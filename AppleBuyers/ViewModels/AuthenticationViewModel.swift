
import Foundation

import Firebase
import FirebaseAuth
import SwiftUI

class AuthenticationViewModel: ObservableObject {
    
    @Published var isSignUpSuccessFull: Bool = true
    @Published var error: String = ""
    @Published var isLoginSuccessFull: Bool = true
    @Published var isLoading = false
    @Published var remembered = false
    @Published var loginSuccess = false
    @Published var male = false
    @Published var isSignUp = false
    @Published var email: String = ""
    @Published var password: String = ""
    @Published var confirmPassword: String = ""
    @Published var resetPassword = false
    
    func logout(){
        do{
            try Auth.auth().signOut()
            UserDBHandler().saveValue(key: UserKeys.isLoggedIn, value: false)
            self.isSignUp = false
            self.loginSuccess = false
            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
                self.clearAllFeilds()
            }
            
            
        }catch{}
    }
    
    func clearAllFeilds(){
        self.email = ""
        self.password = ""
        self.confirmPassword = ""
    }
    
     //MARK:- Forgot password
    func forgotPassword(){
        self.isLoading = true
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            self.resetPassword.toggle()
            self.isLoading = false
            if let error = error{
                self.error = error.localizedDescription
            }else{
                self.error = "Reset password link is sent to your registered email. You can reset password from that link."
            }
        }
    }
    
    //MARK:- Login User
    func login(){
        if email.isEmpty || password.isEmpty{
            DispatchQueue.main.async {
                withAnimation(.easeOut){
                    self.isLoginSuccessFull = false
                    self.error = "Please fill all feilds"
                }
            }
        }else{
            self.isLoading = true
            Auth.auth().signIn(withEmail: email, password: password) { (userData, error) in
                self.isLoading = false
                guard let error = error else{
                   if self.remembered{
                       UserDBHandler().saveValue(key: UserKeys.isLoggedIn, value: true)
                    }
                    DispatchQueue.main.async {
                       self.loginSuccess.toggle()
                    }
                    
                    return
                }
                withAnimation(.easeOut){
                    self.isLoginSuccessFull = false
                    self.error = error.localizedDescription
                }
            }
        }
    }
    
     //MARK:- Create User
    func createUser(){
        if email.isEmpty || password.isEmpty ||  confirmPassword.isEmpty{
            DispatchQueue.main.async {
                withAnimation(.easeOut){
                    self.isSignUpSuccessFull = false
                    self.error = "Please fill all feilds"
                }
            }
           
        }else if password != confirmPassword{
           withAnimation(.easeOut){
            self.isSignUpSuccessFull = false
            self.error = "Password did not matched"
            }
        }else{
            self.isLoading = true
            Auth.auth().createUser(withEmail: email, password: password) { (userData, error) in
                
                guard let error = error else{
                    let model = UserModel()
                    model.email = self.email
                    model.gender = self.male ? "Male" : "Female"
                    model.uid = userData?.user.uid ?? ""
                    
                    FirebaseDBHandler().saveUserData(user: model) { (isSuccess, error) in
                        self.isLoading = false
                        guard let error = error else{
                            self.isSignUp.toggle()
                            return
                        }
                        
                        withAnimation(.easeOut){
                            
                            self.isSignUpSuccessFull = false
                            self.error = error.localizedDescription
                        }
                    }
                    return
                }
                withAnimation(.easeOut){
                    self.isLoading = false
                    self.isSignUpSuccessFull = false
                    self.error = error.localizedDescription
                }
            }
        }
        
    }
}

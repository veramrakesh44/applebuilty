//
//  UserViewModel.swift
//  AppleBuyers
//
//  Created by Rakesh Verma				 on 22/09/20.
//  Copyright © 2020 Rakesh Verma				. All rights reserved.
//

import Foundation

class UserViewModel: ObservableObject{
    @Published var userModel = UserModel()
    @Published var isValid = true
    @Published var message = ""
    @Published var showVerify = false
    @Published var isLoading = false
    
    func sendOTP(){
        if  userModel.phoneNumber.isEmpty{
            isValid = false
            message = "Please Fill Phone number"
        }else if userModel.phoneNumber.count < 10{
            isValid = false
            message = "Phone number should not be less than 10 digits"
        }else{
            self.isLoading.toggle()
            FirebaseDBHandler().sendOTP(phoneNumber: "+91\(userModel.phoneNumber)") { (error) in
                self.isLoading.toggle()
                if let error = error{
                    DispatchQueue.main.async {
                    self.isValid.toggle()
                    self.message = error.localizedDescription
                    }
                }else{
                    DispatchQueue.main.async {
                        self.showVerify.toggle()
                    }
                }
            }
            
        }
    }
    
    
}

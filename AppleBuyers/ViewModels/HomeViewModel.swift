

import Foundation
import SwiftUI


class HomeViewModel: ObservableObject{
    
    @Published var notes:[NoteModel] = []
    @Published var isExpanded: Bool = false
    @Published var isCreateSimpleNote: Bool = false
    @Published var isLoading: Bool = false
    @Published var isProcessCompleted: Bool = false
    @Published var message = ""
    @Published var isCreateAppleBuilityNote: Bool = false
    @Published var isNote: Bool = false{
        didSet{
            self.loadNotes()
        }
    }
    
    init() {
        loadNotes()
    }
    
    func logout(){
        FirebaseDBHandler().logout()
        UserDB.shared.deleteUser()
        
    }
    
    func loadNotes(){
        self.isLoading.toggle()
        FirebaseDBHandler().getNotes(noteType: isNote ? NoteType.Simple : NoteType.AppleBuilty) { (notes, error) in
            guard let error =  error else{
                DispatchQueue.main.async {
                    self.isLoading.toggle()
                    self.notes = notes
                }
                return
            }
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    func showAlert(message: String){
        DispatchQueue.main.async {
            withAnimation(.spring()) {
                self.isLoading.toggle()
                self.isProcessCompleted.toggle()
                self.message = message
            }
           
        }
    }
}

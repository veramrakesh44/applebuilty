
import Foundation
import SwiftUI

class CreateAppleBuilityViewModel: ObservableObject{
    
    @Published var noteModel: NoteModel = NoteModel()
    @Published var noteDateToPerform: Date = Date()
    @Published var isAppleViarities: Bool = true
    @Published var isOptionPopupView = false
    @Published var isBuiltyDetailView = false
    
    @Published var message: String = ""
    @Published var isProcessCompleted: Bool = false
    @Published var isLoading: Bool = false
    
    func validate(){
        if noteModel.title.isEmpty{
            self.isLoading.toggle()
            self.showAlert(message: "Enter Detail Title")
        }else if noteModel.currentVarity.total == "0"{
            self.isLoading.toggle()
            self.showAlert(message: "Please add atleast one size of boxes")
        }else{
            self.noteModel.currentVarity.lotNumber = noteModel.currentVarity.lotNumber
            if isVarietyExist(){
                replaceWithExistingVariety()
            }else{
               self.noteModel.varities.append(self.noteModel.currentVarity)
                noteModel.currentVarity.lotNumber = ""
                           NotificationCenter.default.post(name: NSNotification.clearSizeValues,
                           object: nil, userInfo: nil)
            }
            
            isOptionPopupView.toggle()
           
        }
    }
    
    func isVarietyExist() -> Bool{
        var isAleardyExist = false
        for varity in noteModel.varities{
            if varity.varityName == noteModel.varityName{
                isAleardyExist = true
                break
            }
        }
        return isAleardyExist
    }
   
//    func replaceWithCurrentVariety(){
//
//           for (i,variety) in noteModel.varities.enumerated().reversed(){
//               if variety.varityName == noteModel.currentVarity.varityName{
//                  noteModel.varities[i] =  noteModel.currentVarity
//               }
//           }
//
//       }
    
    func replaceWithExistingVariety(){
        
        for (i,variety) in noteModel.varities.enumerated().reversed(){
            if variety.varityName == noteModel.currentVarity.varityName{
                noteModel.currentVarity = noteModel.varities[i]
            }
        }
        
    }
    
    func validateDetailTextFields(){
        isLoading.toggle()
        noteModel.creationDate = Date().fullDate
        noteModel.modificationDate = Date().fullDate
        noteModel.noteDateToPerform = Date().fullDate
        noteModel.type = .AppleBuilty
        self.isOptionPopupView.toggle()
        FirebaseDBHandler().saveNote(noteType: .AppleBuilty, note: noteModel) { (isSuccess, error) in
            
            guard let error =  error else{
                self.showAlert(message: "Apple Detail Saved successfully. You can check list for viewing detail or edit.")
                self.isBuiltyDetailView.toggle()
                NotificationCenter.default.post(name: NSNotification.dissmissCreateAppleBuiltyView,
                                                object: nil, userInfo: nil)
                return
            }
            self.showAlert(message: error.localizedDescription)
        }
    }
    
    
    func showAlert(message: String){
        DispatchQueue.main.async {
            withAnimation(.spring()) {
                self.isLoading.toggle()
                self.isProcessCompleted.toggle()
                self.message = message
            }
            
        }
    }
    
    func calculateTotal(){
        let extraLarge = Int(noteModel.currentVarity.extraLarge) ?? 0
        let large = Int(noteModel.currentVarity.large) ?? 0
        let small = Int(noteModel.currentVarity.small) ?? 0
        let extraSmall = Int(noteModel.currentVarity.extraSmall) ?? 0
        let medium = Int(noteModel.currentVarity.medium) ?? 0
        let pittu = Int(noteModel.currentVarity.pittu) ?? 0
        let mix = Int(noteModel.currentVarity.mix) ?? 0
        let sevenLyrs = Int(noteModel.currentVarity.sevenLyrs) ?? 0
        let twoFortyD = Int(noteModel.currentVarity.twoFortyD) ?? 0
        
        noteModel.currentVarity.total = "\(extraLarge + large + small + extraSmall + medium + pittu + mix + sevenLyrs + twoFortyD)"
    }
}

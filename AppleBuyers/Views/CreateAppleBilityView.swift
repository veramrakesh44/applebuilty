

import SwiftUI

struct CreateAppleBilityView: View {
    
    @ObservedObject var viewModel = CreateAppleBuilityViewModel()
    @ObservedObject var appleVaritesSizeViewModel = AppleVaritesSizeViewModel()
    @Binding var isCreateAppleBuilityNote: Bool
    
    
    var body: some View {
        ZStack{
            ScrollView{
                VStack{
                    HStack{
                        Button(action: {
                            withAnimation(.spring(response: 0.7, dampingFraction: 0.9, blendDuration: 0.8)) {
                                self.isCreateAppleBuilityNote.toggle()
                            }
                        }) {
                            VStack{
                                Image("more")
                                    .resizable()
                                    .frame(width: 20, height: 20)
                                    .rotationEffect(Angle(degrees: 45) )
                                    .padding()
                            }
                            .modifier(NeumorphicModifier(color: Color(LoginSignUpColors.HeaderColor), radius: 40))
                        }.foregroundColor(Color.white)
                            .padding(.top, 40)
                        Spacer()
                        Text("Create Builty").font(.title).bold()
                            .padding(.top, 40)
                        Spacer()
                        
                        Button(action: {
                            self.viewModel.noteModel.currentVarity = self.appleVaritesSizeViewModel.varietyModel
                            self.viewModel.noteModel.currentVarity.varityName = self.viewModel.noteModel.varityName
                            self.viewModel.validate()
                            
                        }) {
                            Text("Next").font(.title).bold().foregroundColor(Color.white)
                                
                                .frame(width: 80, height: 55)
                                .background(Color(LoginSignUpColors.HeaderColor))
                                .modifier(CustomTextFieldModifier())
                        }.padding(.top, 40)
                            .padding(.trailing, 20)
                        
                    }.padding(.leading, 20)
                    
                    VStack{
                        
                        VStack(alignment: .leading){
                            Text("Title").font(.headline).bold()
                            TextField("Enter Title", text: $viewModel.noteModel.title)
                                .padding()
                                .modifier(CustomTextFieldModifier())
                        }
                        
                        VStack(alignment: .leading){
                            HStack{
                                VStack(alignment: .leading){
                                Text("Varity").font(.headline).bold()
                                Button(action: {
                                    self.viewModel.isAppleViarities.toggle()
                                }) {
                                    HStack{
                                        Text(self.viewModel.noteModel.varityName)
                                        Spacer()
                                        Image("fruit").resizable().frame(width: 20, height: 20)
                                    }.padding()
                                        .modifier(CustomTextFieldModifier())
                                }
                              }
                              VStack(alignment: .leading){
                                Text("Lot Number").font(.headline).bold()
                                TextField("Lot Number", text: $viewModel.noteModel.currentVarity.lotNumber)
                                    
                                    .padding()
                                    .modifier(CustomTextFieldModifier())
                               }.frame(width: 140)
                            }
                           
                        } .padding(.top, 10)
                        
                        AppleVaritiesSizeView(appleVaritesSizeViewModel: appleVaritesSizeViewModel)
                        
                    }.padding()
                    
                    
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .keyboardAware()
            .onTapGesture {
                UIApplication.shared.endEditing()
            }
            .background(Color.white)
            
            //Select Varities View
            if self.viewModel.isAppleViarities{
                AppleVaritiesView(isVarities: $viewModel.isAppleViarities, viewModel: viewModel)
            }
            
            //Option Popup
            if self.viewModel.isOptionPopupView{
                OptionPopUpView(title: "Apple Varity", message: "Do you want to add\none more varity?", viewModel: self.viewModel)
            }
            
            //Builty DetailView
            if self.viewModel.isBuiltyDetailView{
                BuiltyDetailView(viewModel: viewModel)
            }
            
            // Add Error View
            if viewModel.isProcessCompleted{
                ErrorView(title: "Message", message: viewModel.message, isShown: $viewModel.isProcessCompleted)
            }
            
            // Add Loader View
            if viewModel.isLoading{
                ActivityIndicator()
            }
        }
        .edgesIgnoringSafeArea(.all)
        .onReceive(NotificationCenter.default.publisher(for: NSNotification.dissmissCreateAppleBuiltyView)){ _ in
            self.isCreateAppleBuilityNote = false
        }
        .onReceive(NotificationCenter.default.publisher(for: NSNotification.clearSizeValues)){ _ in
           // self.appleVaritesSizeViewModel.varietyModel = AppleVaritySizeModel()
            self.appleVaritesSizeViewModel.clear()
        }
        .onReceive(NotificationCenter.default.publisher(for: NSNotification.reloadSizesWithExistig)){ object in
            if let appleVaritySizeModel = object.object as? AppleVaritySizeModel{
                self.viewModel.noteModel.currentVarity = appleVaritySizeModel
                self.appleVaritesSizeViewModel.varietyModel = appleVaritySizeModel
                self.appleVaritesSizeViewModel.refresh.toggle()
            }
        }
    }
}


class KeyboardInfo: ObservableObject {
    
    public static var shared = KeyboardInfo()
    
    @Published public var height: CGFloat = 0
    
    private init() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIApplication.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardChanged), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    @objc func keyboardChanged(notification: Notification) {
        if notification.name == UIApplication.keyboardWillHideNotification {
            self.height = 0
        } else {
            self.height = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect)?.height ?? 0
        }
    }
    
}

struct KeyboardAware: ViewModifier {
    @ObservedObject private var keyboard = KeyboardInfo.shared
    
    func body(content: Content) -> some View {
        content
            .padding(.bottom, self.keyboard.height)
            .edgesIgnoringSafeArea(self.keyboard.height > 0 ? .bottom : [])
            .animation(.easeOut)
    }
}

extension View {
    public func keyboardAware() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAware())
    }
}




import SwiftUI

struct FloatingButtonView: View {
    
    @ObservedObject var homeViewModel : HomeViewModel
    
    var body: some View {
        HStack{
            Spacer()
            VStack(spacing: 25){
                Spacer()
                VStack(spacing: 25){
                    
                    Button(action: {
                        self.homeViewModel.isExpanded.toggle()
                        self.homeViewModel.isCreateSimpleNote.toggle()
                    }) {
                        VStack{
                            if homeViewModel.isExpanded{
                                Image("edit")
                                    .resizable()
                                    .frame(width: 25, height: 25)
                                    .padding(20)
                            }
                        }.modifier(NeumorphicModifier(color:  Color(LoginSignUpColors.HeaderColor), radius: 40))
                    }.foregroundColor(Color.white)
                    
                    Button(action: {
                        self.homeViewModel.isExpanded.toggle()
                        self.homeViewModel.isCreateAppleBuilityNote.toggle()
                    }) {
                        VStack{
                            if homeViewModel.isExpanded{
                                Image("fruit")
                                    .resizable()
                                    .frame(width: 25, height: 25)
                                    .padding(20)
                            }
                        }.modifier(NeumorphicModifier(color:  Color(LoginSignUpColors.HeaderColor), radius: 40))
                    }.foregroundColor(Color.white)
                }
                
                Button(action: {
                    withAnimation(.spring(response: 0.4, dampingFraction: 0.8, blendDuration: 0.5)) {
                        self.homeViewModel.isExpanded.toggle()
                    }
                }) {
                    VStack{
                        Image("more")
                            .resizable()
                            .frame(width: 35, height: 35)
                            .rotationEffect(homeViewModel.isExpanded ? Angle(degrees: 45) : Angle(degrees: 0))
                            .padding()
                    }
                    .modifier(NeumorphicModifier(color: homeViewModel.isExpanded ?  Color.red: Color(LoginSignUpColors.HeaderColor), radius: 40))
                }.foregroundColor(Color.white)
                
            }
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding(.bottom, 35)
            .padding(.trailing, 35)
            .background(homeViewModel.isExpanded ? Color.black.opacity(0.3) : Color.clear)
            
            .edgesIgnoringSafeArea(.all)
    }
}


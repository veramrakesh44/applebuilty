
import SwiftUI

struct AppleVaritiesSizeView: View {
    
    @ObservedObject var appleVaritesSizeViewModel : AppleVaritesSizeViewModel
    
    var body: some View {
        VStack{
            Text("Total = \(self.appleVaritesSizeViewModel.varietyModel.total)").font(.title).bold()
            HStack{
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel, placeHolder: "L", text: $appleVaritesSizeViewModel.varietyModel.large)
                    .padding(.trailing, 30)
                //Spacer()
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel, placeHolder: "M", text: $appleVaritesSizeViewModel.varietyModel.medium)
            }.padding()
                .modifier(CustomTextFieldModifier())
            
            HStack{
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "S", text: $appleVaritesSizeViewModel.varietyModel.small)
                    .padding(.trailing, 30)
                //Spacer()
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "ES", text: $appleVaritesSizeViewModel.varietyModel.extraSmall)
            }.padding()
                .modifier(CustomTextFieldModifier())
            
            HStack{
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "7Lyrs", text: $appleVaritesSizeViewModel.varietyModel.sevenLyrs)
                    .padding(.trailing, 30)
                //Spacer()
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "240D", text: $appleVaritesSizeViewModel.varietyModel.twoFortyD)
            }.padding()
                .modifier(CustomTextFieldModifier())
            HStack{
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "Pittu", text: $appleVaritesSizeViewModel.varietyModel.pittu)
                    .padding(.trailing, 30)
                // Spacer()
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "Mix", text: $appleVaritesSizeViewModel.varietyModel.mix)
            }.padding()
                .modifier(CustomTextFieldModifier())
            HStack{
                SizeField(appleVaritesSizeViewModel: appleVaritesSizeViewModel,placeHolder: "EL", text: $appleVaritesSizeViewModel.varietyModel.extraLarge)
                //Spacer()
                
            }.padding()
                .modifier(CustomTextFieldModifier())
            
        }
    }
    
}

struct SizeField:View {
    var appleVaritesSizeViewModel: AppleVaritesSizeViewModel
    var placeHolder: String
    @Binding var text: String
    var body: some View{
        VStack{
            Text(placeHolder).font(Font.system(size: 14)).bold()
                .padding(.bottom, -5)
            HStack{
                Button(action: {
                    if let count = Int(self.text), count > 0{
                        self.text = "\((Int(self.text) ?? 0) - 1)"
                    }
                    self.appleVaritesSizeViewModel.varietyModel.calculateTotal()
                }) {
                    PlusMinusView(imageName: "minus")
                }
                TextField("0" + (appleVaritesSizeViewModel.refresh ? "" : " "), text: $text).font(Font.system(size: 15))
                    .frame(width: 25, height: 25)
                    .multilineTextAlignment(.center)
                    .keyboardType(.numberPad)
                    .padding()
                    .modifier(CustomTextFieldModifier())
                
                Button(action: {
                    self.text = "\((Int(self.text) ?? 0) + 1)"
                    self.appleVaritesSizeViewModel.varietyModel.calculateTotal()
                }) {
                    PlusMinusView(imageName: "more")
                }
            }
        }
    }
}

struct PlusMinusView:View {
    var imageName: String
    var body: some View{
        Image(imageName)
            .renderingMode(.original)
            .resizable()
            .frame(width: 15, height: 15)
            .padding(5)
            .background(Color(LoginSignUpColors.HeaderColor))
            .clipShape(Circle())
        .modifier(CustomTextFieldModifier())
    }
}

class AppleVaritesSizeViewModel: ObservableObject{
    @Published  var refresh = false
    @Published var varietyModel = AppleVaritySizeModel()

    
    func clear(){
        self.varietyModel = AppleVaritySizeModel()
        self.refresh.toggle()
        
    }
}

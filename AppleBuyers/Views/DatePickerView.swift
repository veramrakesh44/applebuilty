
import SwiftUI

struct DatePickerView: View {
    @ObservedObject var createNoteViewModel: CreateNoteViewModel
    var body: some View {
        VStack(alignment: .leading){
            Text("Date/Time").font(.headline).bold()
            DatePicker(selection: $createNoteViewModel.noteDateToPerform, displayedComponents: .date) {
                Text("")
            }
        }
    }
}


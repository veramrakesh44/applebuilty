

import SwiftUI

struct OptionPopUpView: View {
    
    var title: String = ""
    var message: String = ""
    @ObservedObject var viewModel: CreateAppleBuilityViewModel
    var body: some View {
        ZStack{
            VStack(alignment: .leading){
                Text(title).font(.largeTitle)
                    .frame(width: UIScreen.main.bounds.size.width - 60, height: 45)
                    .padding(.top, 20)
            
                Text(message).font(.headline)
                    .frame(width: UIScreen.main.bounds.size.width - 60, height: 45)
                    .padding()
                    
                VStack(alignment: .center){
                    HStack{
                        Button(action: {
                            withAnimation(.easeIn){
                                self.viewModel.isBuiltyDetailView = false
                                self.viewModel.isAppleViarities = true
                                 self.viewModel.isOptionPopupView.toggle()
                            }
                           
                        }) {
                            Text("Yes").font(.title)
                                .foregroundColor(Color.white)
                                .frame(width: UIScreen.main.bounds.size.width / 2 - 40, height: 45)
                                .background(Color(LoginSignUpColors.HeaderColor))
                             .cornerRadius(15)
                        }
                        Button(action: {
                            withAnimation(.easeIn){
                                self.viewModel.isBuiltyDetailView = true
                                self.viewModel.isAppleViarities = false
                                self.viewModel.isOptionPopupView.toggle()
                            }
                           
                        }) {
                            Text("No").font(.title)
                                .foregroundColor(Color.white)
                                .frame(width: UIScreen.main.bounds.size.width / 2 - 40, height: 45)
                                .background(Color(LoginSignUpColors.HeaderColor))
                             .cornerRadius(15)
                        }
                    }
                    
                }.padding()
            }
            
            .background(Color.white)
                .cornerRadius(15)
            .padding()
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.black.opacity(0.5))
            
    
        
    }
}

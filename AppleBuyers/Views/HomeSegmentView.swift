

import SwiftUI

struct HomeSegmentView: View {
    
    @ObservedObject var viewModel: HomeViewModel
    
    var body: some View{
        ZStack{
            Text("")
                .frame(width: 122, height: 45)
               
                .background(Color(LoginSignUpColors.HeaderColor))
                .cornerRadius(22.5)
             .offset(x: self.viewModel.isNote ? 61 : -61)
            
            HStack(spacing: 0){
     
                Button(action: {
                    withAnimation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0)) {
                        self.viewModel.isNote = false
                    }
                }) {
                    Text("Apple Details")
                        .foregroundColor(!self.viewModel.isNote ? Color.white : Color(LoginSignUpColors.HeaderColor))
                        .frame(width: 122, height: 45)
                }
                
                Button(action: {
                    withAnimation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0)) {
                        self.viewModel.isNote = true
                    }
                }) {
                    Text("Notes")
                        .foregroundColor(self.viewModel.isNote ? Color.white : Color(LoginSignUpColors.HeaderColor))
                        .frame(width: 122, height: 45)
                    
                }
                
            }
        }.frame(width:250, height: 55)
        .background(Color(LoginSignUpColors.loginBackground))
        .cornerRadius(28)
        .shadow(color: Color.black.opacity(0.2), radius: 3, x: 3, y: 3)
        .shadow(color: Color.black.opacity(0.2), radius: 3, x: -3, y: -3)
        .padding(.top, 10)
        
        
    }
}

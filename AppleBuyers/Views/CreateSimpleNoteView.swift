
import SwiftUI

struct CreateSimpleNoteView: View {
    @ObservedObject var viewModel = CreateNoteViewModel()
    @Binding var isCreateSimpleNote: Bool
    var body: some View {
        ZStack{
            ScrollView(.vertical){
                HStack{
                Button(action: {
                    withAnimation(.spring(response: 0.7, dampingFraction: 0.9, blendDuration: 0.8)) {
                        self.isCreateSimpleNote.toggle()
                    }
                }) {
                    VStack{
                        Image("more")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .rotationEffect(Angle(degrees: 45) )
                            .padding()
                    }
                    .modifier(NeumorphicModifier(color: Color(LoginSignUpColors.HeaderColor), radius: 40))
                }.foregroundColor(Color.white)
                    .padding(.top, 50)
                    Spacer()
                }.padding(.leading, 20)
                
                VStack(spacing: 20){
                    HStack{
                        Spacer()
                        Text("Create Note").font(.largeTitle).bold()
                        Spacer()
                    }.padding()
                    
                    VStack(alignment: .leading, spacing: 10){
                        Text("Title").font(.headline).bold()
                        TextField("Title", text: $viewModel.noteModel.title)
                            .padding()
                            .modifier(CustomTextFieldModifier())
                    }
                    
                    VStack(alignment: .leading, spacing: 10){
                        Text("Detail").font(.headline).bold()
                        TextField("Detail", text: $viewModel.noteModel.detail)
                            .padding()
                            .modifier(CustomTextFieldModifier())
//                        TextView(text: $viewModel.detailText)
//                            .frame(height: 300)
//                            .padding()
//                            .modifier(CustomTextFieldModifier())
                    }
                    
                  DatePickerView(createNoteViewModel: viewModel)
            
                    Toggle("",isOn: $viewModel.noteModel.isReminderOn).toggleStyle(ColoredToggleStyle( onColor: Color(LoginSignUpColors.HeaderColor)))
                    
                    Button(action: {
                     self.viewModel.saveSimpleNote()
                    }) {
                        Text("Create").font(.title).bold().foregroundColor(Color.white)
                            
                            .frame(width: 250, height: 55)
                            .background(Color(LoginSignUpColors.HeaderColor))
                            .clipShape(Capsule())
                            
                            .shadow(color: Color.black.opacity(0.5), radius: 4, x: 4, y: 4)
                    }.padding(.top, 20)
                    .padding(.bottom, 20)
                }.padding(.trailing, 20)
                   .padding(.leading, 20)
                }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.white)
                
            // Add Error View
            if viewModel.isProcessCompleted{
                ErrorView(title: "Message", message: viewModel.message, isShown: $viewModel.isProcessCompleted)
            }
            
            // Add Loader View
            if viewModel.isLoading{
                ActivityIndicator()
            }
        }

            
        .edgesIgnoringSafeArea(.all)
    }
}

struct CustomTextFieldModifier: ViewModifier {
    func body(content: Content) -> some View {
        content.background(Color.black.opacity(0.05))
        .cornerRadius(15)
        .overlay(
            RoundedRectangle(cornerRadius: 15)
                .stroke(Color.black.opacity(0.05), lineWidth: 4)
                .shadow(color: Color.black.opacity(0.2), radius: 6, x: 6, y: 6)
            .clipShape(RoundedRectangle(cornerRadius: 15))
            .shadow(color: Color.black.opacity(0.2), radius: 6, x: -6, y: -6)
             .clipShape(RoundedRectangle(cornerRadius: 15))
        )
    }
}


struct ColoredToggleStyle: ToggleStyle {
    var onColor = Color(UIColor.green)
    var offColor = Color(UIColor.systemGray5)
    var thumbColor = Color.white
    
    func makeBody(configuration: Self.Configuration) -> some View {
        HStack {
            Image("notification").resizable()
                .frame(width: 16, height: 16)
                .padding(10)
                .background(Color(LoginSignUpColors.HeaderColor))
                .clipShape(Circle())
            Text("Reminder").font(.headline).bold()
            
           
            Button(action: { configuration.isOn.toggle() } )
            {
                RoundedRectangle(cornerRadius: 16, style: .circular)
                    .fill(configuration.isOn ? onColor : offColor)
                    .frame(width: 50, height: 29)
                    .overlay(
                        Circle()
                            .fill(thumbColor)
                            .shadow(radius: 1, x: 0, y: 1)
                            .padding(1.5)
                            .offset(x: configuration.isOn ? 10 : -10))
                    .animation(Animation.easeInOut(duration: 0.1))
            }
             Spacer()
        }
        //.font(.title)
        //.padding(.horizontal)
    }
}


struct TextView: UIViewRepresentable {
    @Binding var text: String

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    func makeUIView(context: Context) -> UITextView {

        let myTextView = UITextView()
        myTextView.delegate = context.coordinator

        myTextView.font = UIFont(name: "HelveticaNeue", size: 15)
        myTextView.isScrollEnabled = true
        myTextView.isEditable = true
        myTextView.isUserInteractionEnabled = true
       

        return myTextView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
    }

    class Coordinator : NSObject, UITextViewDelegate {

        var parent: TextView

        init(_ uiTextView: TextView) {
            self.parent = uiTextView
        }

        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            return true
        }

        func textViewDidChange(_ textView: UITextView) {
            print("text now: \(String(describing: textView.text!))")
            self.parent.text = textView.text
        }
    }
}


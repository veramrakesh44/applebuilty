//
//  VerifyPhoneNumberView.swift
//  AppleBuyers
//
//  Created by Rakesh Verma				 on 22/09/20.
//  Copyright © 2020 Rakesh Verma				. All rights reserved.
//

import SwiftUI

struct VerifyPhoneNumberView: View {
    
   @ObservedObject var viewModel = VeryPhoneViewModel()
    @Binding var hideView: Bool
    var phoneNumber: String
    
    var body: some View {
        ZStack{
            
        
            VStack{
                LandingPageHeader()
                VStack(alignment: .leading){
                    Text("Verify phone number").font(.title).bold()
                        .padding(.top, 20)
                    Text("Check your SMS messages. We have sent\nthe pin at \(phoneNumber)").font(.body)
                }.frame(maxWidth: .infinity)
                
                VStack(spacing: 5){
                    HStack{
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.first, alignmnent: .center)
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.second, alignmnent: .center)
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.third, alignmnent: .center)
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.fourth, alignmnent: .center)
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.fifth, alignmnent: .center)
                        BuiltyDetailTextField(placeHolder: "", text: self.$viewModel.otp.sixth, alignmnent: .center)
                    }
                    
                    HStack{
                        Text("Didn't receive sms?")
                        
                        Button(action: {
                            
                        }) {
                            Text("Resend Code").font(.headline).bold().foregroundColor(Color(LoginSignUpColors.HeaderColor))
                                .frame(height: 45)
                                .multilineTextAlignment(.leading)
                        }
                    }
                    
                    HStack{
                        Button(action: {
                            withAnimation(.spring(response: 0.7, dampingFraction: 0.9, blendDuration: 0.8)) {
                                self.hideView.toggle()
                            }
                        }) {
                            Text("Back").font(.headline).bold().foregroundColor(Color.white)
                                .frame(width: 130, height: 55)
                                .background(Color(LoginSignUpColors.HeaderColor))
                                .modifier(CustomTextFieldModifier())
                        }
                        
                        Button(action: {
                            self.viewModel.login()
                        }) {
                            Text("Verify").font(.headline).bold().foregroundColor(Color.white)
                                .frame(width: 130, height: 55)
                                .background(Color(LoginSignUpColors.HeaderColor))
                                .modifier(CustomTextFieldModifier())
                        }
                    }
                    
                    
                   
                }.padding()
              
                
                Spacer()
            }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .keyboardAware()
          
            if viewModel.isError{
                ErrorView(title: "Error!", message: viewModel.message, isShown: $viewModel.isError)
            }
            if viewModel.isLoading{
                ActivityIndicator()
            }
            if viewModel.isLogInSuccess{
                NavigationLink( "", destination: HomeView(), isActive: $viewModel.isLogInSuccess)
            }
        }
        .navigationBarBackButtonHidden(true)
        .edgesIgnoringSafeArea(.all)
    }
}

class VeryPhoneViewModel: ObservableObject{
    @Published var otp = OTP()
    @Published var message = ""
    @Published var isLoading = false
    @Published var isLogInSuccess = false
    @Published var isError = false
    
    func login(){
        if otp.number.count != 6{
            isError = true
            message = "Please fill all fields."
        }else{
            self.isLoading.toggle()
            FirebaseDBHandler().signInWithPhonenumber(verificationCode: otp.number) { (error) in
                self.isLoading.toggle()
                if let error = error{
                    self.isError = true
                    self.message = error.localizedDescription
                }else{
                    self.isLogInSuccess.toggle()
                    UserDB.shared.isLoggedIn = true
                }
            }
        }
    }
}

class OTP: Identifiable{
    var first = ""{
        didSet{
            concatNumber()
        }
    }
    var second = ""{
        didSet{
            concatNumber()
        }
    }
    var third = ""
    {
        didSet{
            concatNumber()
        }
    }
    var fourth = ""{
        didSet{
            concatNumber()
        }
    }
    var fifth = ""{
        didSet{
            concatNumber()
        }
    }
    var sixth = ""{
        didSet{
            concatNumber()
        }
    }
    
    var number = ""
    func concatNumber(){
        number = "\(first)\(second)\(third)\(fourth)\(fifth)\(sixth)"
    }
}

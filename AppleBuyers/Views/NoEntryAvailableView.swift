

import SwiftUI

struct NoEntryAvailableView: View {
    var body: some View {
        VStack{
            Text("No Entry Available")
                .font(.largeTitle)
                .bold()
                .padding()
            
            Text("Click on plus button to")
                .font(.headline)
                .foregroundColor(Color.black.opacity(0.8))
            Text("create new entry ")
            .font(.headline)
            .foregroundColor(Color.black.opacity(0.8))
                .padding(.top, 10)
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

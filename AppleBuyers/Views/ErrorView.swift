

import SwiftUI

struct ErrorView: View {
    
    var title: String = ""
    var message: String = ""
    @Binding var isShown: Bool
    
    var body: some View {
        ZStack{
            VStack(alignment: .leading){
                Text(title).font(.largeTitle)
                    .frame(width: UIScreen.main.bounds.size.width - 60, height: 45)
                    .padding(.top, 20)
            
                Text(message).font(.headline)
                    .frame(width: UIScreen.main.bounds.size.width - 60, height: 45)
                    .padding()
                    
                VStack(alignment: .center){
                    Button(action: {
                        withAnimation(.easeIn){
                             self.isShown.toggle()
                        }
                       
                    }) {
                        Text("OK").font(.title)
                            .foregroundColor(Color.white)
                            .frame(width: UIScreen.main.bounds.size.width - 60, height: 45)
                            .background(Color(LoginSignUpColors.HeaderColor))
                         .cornerRadius(15)
                    }
                }.padding()
            }
            
            .background(Color.white)
                .cornerRadius(15)
            .padding()
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.black.opacity(0.5))
            
    
        
    }
}


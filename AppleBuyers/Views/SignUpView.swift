
import SwiftUI

struct SignUpView: View {
 
    @ObservedObject var authenticationViewModel: AuthenticationViewModel
    
    var body: some View{
        ZStack{
            VStack{
                Spacer()
                ZStack{
                    
                    ZStack{
                        Color.white
                        // .padding()
                        }.frame(width: 90, height: 90)  .background(Color.white)
                        .cornerRadius(45)
                        .shadow(color: Color.black.opacity(0.4), radius: 4, x: 4, y: 4)
                        .offset( y: 250)
                    VStack{
                        Text("Welcome").font(Font.system(size: 40, weight: .heavy, design: .rounded)).foregroundColor(Color.white)
                        Text("Continue to Sign Up")
                            .foregroundColor(Color.white)
                            .font(Font.system(size: 16))
                            .padding(.bottom, 20)
                        VStack{
                                    HStack{
                                         CustomSegmentView(authenticationViewModel: authenticationViewModel)
                                    }.frame(height: 80)
                                    CustomTextField(feildText: $authenticationViewModel.email, placeHolderText: "Email",  imageName: "email", isSecure: false)
                                        .padding(.leading, 20)
                                        .padding(.trailing, 20)
                                    CustomTextField(feildText: $authenticationViewModel.password, placeHolderText: "Password",  imageName: "password", isSecure: true)
                                    .padding(.leading, 20)
                                    .padding(.trailing, 20)
                                    CustomTextField(feildText: $authenticationViewModel.confirmPassword, placeHolderText: "Confirm Password",  imageName: "password", isSecure: true)
                                        .padding(.leading, 20)
                                        .padding(.trailing, 20)
                                        .padding(.bottom, 10)
                                    HStack(spacing: 20){
                                        Button(action: {
                                            self.authenticationViewModel.male = true
                                            
                                        }) {
                                            HStack{
                                                Image("male")
                                                    .renderingMode(.original)
                                                    .resizable()
                                                    .frame(width: 20, height: 20)
                                                Text("Male")
                                                
                                                    .foregroundColor(!self.authenticationViewModel.male ? Color.black.opacity(0.5) :  Color.white)
                                                    .font(Font.system(size: 15))
                                            }
                                             .padding()
                                            .background(self.authenticationViewModel.male ? Color(LoginSignUpColors.HeaderColor) : Color.clear)
                                        .cornerRadius(12)
                                           
                                        }.padding(.leading, 5)
                                       
                                        Button(action: {
                                            self.authenticationViewModel.male = false
                                        }) {
                                            HStack{
                                                Image("female")
                                                    .renderingMode(.original)
                                                    .resizable()
                                                    .frame(width: 20, height: 20)
                                                Text("Female")
                                                    .foregroundColor(self.authenticationViewModel.male ? Color.black.opacity(0.5) :  Color.white)
                                                    .font(Font.system(size: 15))
                                                
                                            }
                                            .padding()
                                            .background(self.authenticationViewModel.male ? Color.clear : Color(LoginSignUpColors.HeaderColor))
                                            .cornerRadius(12)
                                        }
                                        Spacer()
                                        
                                    }.padding(.leading, 20)
                                        .padding(.trailing, 20)
                                    
                                    Spacer()
                                    
                                }.frame(width: UIScreen.main.bounds.size.width - 40, height: 400)
                                .background(Color.white)
                                .cornerRadius(25)
                                .shadow(color: Color.black.opacity(0.3)
                                    , radius: 5, x: 5, y: 5)
                                    .shadow(color: Color.black.opacity(0.2)
                                        , radius: 5, x: -5, y: -5)
                    }
        
                    
                    ZStack{
                        ZStack{
                            //Sign up action
                            Button(action: {
                                self.authenticationViewModel.createUser()
                            }) {
                                Image("forwardarrow")
                                    .resizable()
                                    .frame(width: 40, height: 40)
                            }
                            
                        }
                        .frame(width: 60, height: 60)
                        .foregroundColor(Color.white)
                        .background(LinearGradient(gradient: .init(colors: [Color(LoginSignUpColors.LoginSeconday ), Color(LoginSignUpColors.LoginPrimary )]), startPoint: .leading, endPoint: .trailing))
                            
                        .cornerRadius(30)
                        .shadow(color: Color.black.opacity(0.2), radius: 2, x: 2, y: 2)
                        .shadow(color: Color.black.opacity(0.1), radius: 2, x: -2, y: -2)
                        
                    }.frame(width: 90, height: 90)
                        .background(Color.white)
                        .cornerRadius(45)
                        .offset( y: 250)
                }.frame(width: UIScreen.main.bounds.size.width - 40, height: 450)
                Spacer()
                VStack(spacing: 40){
                    Text("Or Signup with")
                        .foregroundColor(Color.black.opacity(0.5))
                    HStack{
                        HStack{
                            Image("facebook")
                                .resizable()
                                .frame(width: 20, height: 20)
                            Text("Facebook")
                                .foregroundColor(Color.white)
                                .frame(width: 100)
                        }.frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 50)
                            .background(Color(LoginSignUpColors.HeaderColor))
                            .cornerRadius(25)
                            .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
                        
                        HStack{
                            Image("google")
                                .resizable()
                                .frame(width: 20, height: 20)
                            Text("Google")
                                .foregroundColor(Color.white)
                                .frame(width: 100)
                            
                        }.frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 50)
                            .background(Color(LoginSignUpColors.GoogleButtonBackground))
                            .cornerRadius(25)
                            .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
                    }.frame(width: UIScreen.main.bounds.size.width - 50)
                }.padding()
                    .padding(.bottom, 30)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            if !authenticationViewModel.isSignUpSuccessFull{
                ErrorView(title: "Error", message: authenticationViewModel.error, isShown: $authenticationViewModel.isSignUpSuccessFull)
            }
            if authenticationViewModel.isLoading{
                ActivityIndicator()
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
     
    }
}

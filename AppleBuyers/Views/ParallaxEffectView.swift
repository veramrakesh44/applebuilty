
import SwiftUI

struct ParallaxEffectView: View {
    var body: some View {
        ScrollView{
            VStack{
                
                GeometryReader{geo in
                    
                   Image("india").resizable()
                    .offset( y: -geo.frame(in: .global).minY)
                    .frame(width: UIScreen.main.bounds.size.width, height: geo.frame(in: .global).minY > 0  ? geo.frame(in: .global).minY + 300 : 300)
                }.frame(height: 300)
                
                VStack{
                    Text("Indian Tutorist Places").font(.title).bold().padding(.top, 15)
                    
                    HStack{
                        Text("Images").font(.headline).bold()
                        Spacer()
                    }.padding(.leading, 15)
                    .padding(.top, 30)
                    
                    HStack(spacing: 10){
                        Image("india1").resizable()
                            .frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 80)
                        Image("india2").resizable()
                        .frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 80)
                    }
                    
                    HStack{
                        Text("About").font(.headline).bold()
                        Spacer()
                    }.padding(.leading, 15)
                        .padding(.top, 15)
                    
                    HStack{
                        Text(about).font(.body).foregroundColor(Color.black.opacity(0.7))
                        Spacer()
                    }.padding(.leading, 15)
                        .padding(.top, 15)
                    
                }.background(Color.white)
                .cornerRadius(15)
                .offset( y: -50)
            }
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .edgesIgnoringSafeArea(.all)
    }
    
}


let about = "India is a vibrant land of startling contrasts where both the traditional and modern worlds meet. The world's seventh largest nation by area and the second largest in terms of population, India boasts a rich heritage that's the result of centuries of different cultures and religions leaving their mark. Things to do for travelers include the opportunity to experience an array of sacred sites and spiritual encounters, while nature lovers will enjoy its sun-washed beaches, lush national parks, and exciting wildlife sanctuaries.From the magnificent Taj Mahal in Agra to the holy sites of Harmandir Sahib (formerly the Golden Temple) in Amritsar and the Mecca Masjid mosque in Hyderabad, visitors to this exotic country will discover a trove of spiritual, cultural, and historical treasures."

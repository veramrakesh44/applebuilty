//
//  PhoneNumberLoginView.swift
//  AppleBuyers
//
//  Created by Rakesh Verma				 on 22/09/20.
//  Copyright © 2020 Rakesh Verma				. All rights reserved.
//

import SwiftUI

struct PhoneNumberLoginView: View {
    @ObservedObject var viewModel = UserViewModel()
    var body: some View {
        NavigationView{
            ZStack{
                VStack{
                    LandingPageHeader()
                    VStack(spacing: 20){
                        Text("Enter Number").font(.title).bold()
                            .padding(.top, 20)
                        
                        BuiltyDetailTextField(placeHolder: "Phone number", text: self.$viewModel.userModel.phoneNumber, isTopHidden: true, keyboardType: UIKeyboardType.phonePad)
                        
                        NavigationLink( "", destination: VerifyPhoneNumberView(hideView: $viewModel.showVerify, phoneNumber: viewModel.userModel.phoneNumber), isActive: $viewModel.showVerify)
                        
                        Button(action: {
                            viewModel.sendOTP()
                        }) {
                            Text("Next").font(.headline).bold().foregroundColor(Color.white)
                                .frame(width: 200, height: 55)
                                .background(Color(LoginSignUpColors.HeaderColor))
                                .modifier(CustomTextFieldModifier())
                        }
                    }.padding()
                  
                    
                    Spacer()
                }.frame(maxWidth: .infinity, maxHeight: .infinity)
                .keyboardAware()
                
                if !viewModel.isValid{
                    ErrorView(title: "Error!", message: viewModel.message, isShown: $viewModel.isValid)
                }
                if viewModel.isLoading{
                    ActivityIndicator()
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .navigationBarHidden(true)
            .edgesIgnoringSafeArea(.all)
            
        }
        
        .onTapGesture(count: 1, perform: {
            UIApplication.shared.endEditing()
        })
    }
    
}

struct LandingPageHeader: View {
    var body: some View{
        VStack{
            VStack{
                VStack{
                    Image("cortLandApple").resizable().frame(width: 60, height: 60)
                    Text("Welcome").font(.title).bold()
                }.padding()
                .background(Color.white)
                .modifier(CustomTextFieldModifier())
                
                Text("No need to remember").font(.headline).bold().foregroundColor(.white)
                    .padding(.top, 15)
            }.padding(.top, 65)
            
        }.frame(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height / 2)
        .background(Color(LoginSignUpColors.HeaderColor))
        .clipShape(CsutomShape(corner: [.bottomRight,.bottomLeft], radii: 35))
    }
}

struct PhoneNumberLoginView_Previews: PreviewProvider {
    static var previews: some View {
        PhoneNumberLoginView()
    }
}

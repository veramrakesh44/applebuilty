

import SwiftUI

struct AppleVaritiesView: View {
    
    @Binding var isVarities: Bool
    @ObservedObject var viewModel: CreateAppleBuilityViewModel
    var appleVarities = ["Royal","Golden","Spur"]
    
    var body: some View {
        VStack{
            
            VStack(alignment: .center){
                HStack{
                    Spacer()
                    Text("Select Varity").font(.headline).bold()
                    Spacer()
                }
                List{
                    ForEach(0..<appleVarities.count){index in
                        Button(action: {
                            self.viewModel.noteModel.currentVarity = AppleVaritySizeModel()
                            self.viewModel.noteModel.currentVarity.varityName = self.appleVarities[index]
                             self.viewModel.noteModel.varityName = self.appleVarities[index]
                            if self.viewModel.isVarietyExist(){
                                self.viewModel.replaceWithExistingVariety()
                                
                                NotificationCenter.default.post(name: NSNotification.reloadSizesWithExistig,
                                                                object: self.viewModel.noteModel.currentVarity, userInfo: nil)
                            }else{
                                NotificationCenter.default.post(name: NSNotification.reloadSizesWithExistig,
                                object: AppleVaritySizeModel(), userInfo: nil)
                            }
                            self.isVarities = false
                        }) {
                            Text(self.appleVarities[index])
                        }.tag(index)
                    }
                }
            }.frame(width: 300, height: 300)
                .padding()
                .background(Color.white)
                .cornerRadius(20)
            
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color.black.opacity(0.5))
    }
}




import SwiftUI

struct BuiltyDetailView: View {
    @ObservedObject var viewModel: CreateAppleBuilityViewModel
    
    var body: some View {
             ScrollView{
                   VStack{
                       HStack{
                           Button(action: {
                               withAnimation(.spring(response: 0.7, dampingFraction: 0.9, blendDuration: 0.8)) {
                                self.viewModel.isBuiltyDetailView.toggle()
                               }
                           }) {
                               VStack{
                                   Image("more")
                                       .resizable()
                                       .frame(width: 20, height: 20)
                                       .rotationEffect(Angle(degrees: 45) )
                                       .padding()
                               }
                               .modifier(NeumorphicModifier(color: Color(LoginSignUpColors.HeaderColor), radius: 40))
                           }.foregroundColor(Color.white)
                               .padding(.top, 40)
                           Spacer()
                           Text("Enter Detail").font(.title).bold()
                               .padding(.top, 40)
                           Spacer()
                           
                           Button(action: {
                               
                            self.viewModel.validateDetailTextFields()
                           }) {
                               Text("Save").font(.title).bold().foregroundColor(Color.white)
                                   
                                   .frame(width: 80, height: 55)
                                   .background(Color(LoginSignUpColors.HeaderColor))
                                   .modifier(CustomTextFieldModifier())
                           }.padding(.top, 40)
                               .padding(.trailing, 20)
                           
                       }.padding(.leading, 20)
                       
                    VStack{
                        BuiltyDetailTextField(placeHolder: "Sender", text: self.$viewModel.noteModel.senderName)
                        BuiltyDetailTextField(placeHolder: "Consignee", text: self.$viewModel.noteModel.consigneeName)
                        BuiltyDetailTextField(placeHolder: "Owner's Name", text: self.$viewModel.noteModel.ownersName)
                        
                        BuiltyDetailTextField(placeHolder: "Prop", text: self.$viewModel.noteModel.prop)
                        HStack{
                            BuiltyDetailTextField(placeHolder: "Truck Number", text: self.$viewModel.noteModel.truckNumber)
                            BuiltyDetailTextField(placeHolder: "Freight", text: self.$viewModel.noteModel.freight)
                        }
                        
                        HStack{
                            BuiltyDetailTextField(placeHolder: "From", text: self.$viewModel.noteModel.fromLocation)
                            BuiltyDetailTextField(placeHolder: "To", text: self.$viewModel.noteModel.toLocation)
                        }
                        VStack(alignment: .leading){
                            Text("Date/Time").font(.headline).bold()
                            DatePicker(selection: $viewModel.noteModel.builtyDate, displayedComponents: .date) {
                                Text("")
                            }
                        }
                    }.padding()
                       
                       
                   }
               }
               .frame(maxWidth: .infinity, maxHeight: .infinity)
               .keyboardAware()
               .onTapGesture {
                   UIApplication.shared.endEditing()
               }
                
               .background(Color.white)
    }
}


struct BuiltyDetailTextField: View {
    var placeHolder: String
    @Binding var text: String
    var alignmnent: TextAlignment = .leading
    var isTopHidden = false
    var keyboardType: UIKeyboardType = .default
    var body: some View{
        VStack(alignment: .leading){
            if !isTopHidden{
            Text(placeHolder).font(.headline).bold()
            }
            TextField(placeHolder, text: $text)
                .keyboardType(keyboardType)
                .multilineTextAlignment(alignmnent)
                .padding()
                .modifier(CustomTextFieldModifier())
                
        }
    }
}

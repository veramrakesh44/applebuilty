

import SwiftUI

struct LoginSignUpColors {
    static let GoogleButtonBackground = "GoogleButtonBackground"
     static let HeaderColor = "HeaderColor"
     static let HeaderYellowTitle = "HeaderYellowTitle"
     static let loginBackground = "loginBackground"
     static let LoginPrimary = "LoginPrimary"
    static let LoginSeconday = "LoginSeconday"
}

struct ContentView: View {
    @ObservedObject var authenticationViewModel = AuthenticationViewModel()
    var body: some View {
        ZStack{
            Color(LoginSignUpColors.loginBackground)
            
            //Creating Header View
            VStack{
                VStack{
                    Color(LoginSignUpColors.HeaderColor)
                }
                .frame(maxWidth: .infinity,maxHeight: 350)
                .clipShape(CsutomShape(corner: .bottomLeft, radii: 60))
                Spacer()
            }.frame(maxHeight: .infinity)
           
            //Creating Login View
            if authenticationViewModel.isSignUp{
                SignUpView(authenticationViewModel: authenticationViewModel)
            }else{
                LoginView(authenticationViewModel: authenticationViewModel)
               
            }
            if authenticationViewModel.loginSuccess || UserDBHandler().isLoggedIn(){
                HomeView()
            }
            
        }
        .edgesIgnoringSafeArea(.all)
    }
   
    
    func setRootView(){
        let scene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        
        if let widowSceneDelegate = scene?.delegate  as? SceneDelegate{
            let window = UIWindow(windowScene: scene!)
            
            window.rootViewController = UIHostingController(rootView: HomeView())
            widowSceneDelegate.window = window
            window.makeKeyAndVisible()
        }
    }
}
extension NSNotification {
    static let SegmentClicked = NSNotification.Name.init("SegmentClicked")
    static let LoginSuccessFul = NSNotification.Name.init("LoginSuccessFul")
     static let logout = NSNotification.Name.init("logout")
    static let dissmissCreateAppleBuiltyView = NSNotification.Name.init("dissmissCreateAppleBuiltyView")
    static let clearSizeValues = NSNotification.Name.init("clearSizeValues")
    static let reloadSizesWithExistig = NSNotification.Name.init("reloadSizesWithExistig")
}

struct CustomSegmentView: View {
     @ObservedObject var authenticationViewModel: AuthenticationViewModel
    var body: some View{
        HStack(spacing: 0){
            Button(action: {
                withAnimation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0)) {
                     self.authenticationViewModel.clearAllFeilds()
                    self.authenticationViewModel.isSignUp = false
                }
            }) {
               Text("Login In")
                .foregroundColor(!authenticationViewModel.isSignUp ? Color.white : Color(LoginSignUpColors.HeaderColor))
                .frame(width: 90, height: 45)
                .background(!authenticationViewModel.isSignUp ? Color(LoginSignUpColors.HeaderColor) : Color.clear)
                .cornerRadius(25)
            }
            
            Button(action: {
          
                withAnimation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 0)) {
                    self.authenticationViewModel.clearAllFeilds()
                    self.authenticationViewModel.isSignUp = true
                }
                
            }) {
               Text("Sign Up")
                .foregroundColor(authenticationViewModel.isSignUp ? Color.white : Color(LoginSignUpColors.HeaderColor))
                .frame(width: 90, height: 45)
                
                .background(authenticationViewModel.isSignUp ? Color(LoginSignUpColors.HeaderColor) : Color.clear)
                .cornerRadius(25)
            }
            
        } .frame(width:185, height: 55)
            .background(Color(LoginSignUpColors.loginBackground))
        .cornerRadius(28)
            .shadow(color: Color.black.opacity(0.2), radius: 3, x: 3, y: 3)
        .shadow(color: Color.black.opacity(0.2), radius: 3, x: -3, y: -3)
      
    }
}

struct LoginView: View {
   
    @ObservedObject var authenticationViewModel: AuthenticationViewModel
    
    var body: some View{
        ZStack{
            VStack{
                Spacer()
                ZStack{
                    
                    ZStack{
                        Color.white
                        // .padding()
                    }.frame(width: 90, height: 90)
                        
                        .background(Color.white)
                        .cornerRadius(45)
                        .shadow(color: Color.black.opacity(0.4), radius: 4, x: 4, y: 4)
                        .offset( y: 190)
                    VStack{
                        Text("Welcome").font(Font.system(size: 40, weight: .heavy, design: .rounded)).foregroundColor(Color.white)
                        Text("Continue to Sign In")
                            .foregroundColor(Color.white)
                            .font(Font.system(size: 16))
                            .padding(.bottom, 20)
                        VStack{
                            HStack{
                                CustomSegmentView(authenticationViewModel: authenticationViewModel)
                            }.frame(height: 80)
                            CustomTextField(feildText: $authenticationViewModel.email, placeHolderText: "Email",  imageName: "email", isSecure: false)
                                .padding(.leading, 20)
                                .padding(.trailing, 20)
                            CustomTextField(feildText: $authenticationViewModel.password, placeHolderText: "Password",  imageName: "password", isSecure: true)
                                .padding(.leading, 20)
                                .padding(.trailing, 20)
                                .padding(.top, 20)
                                .padding(.bottom, 10)
                            HStack{
                                Button(action: {
                                    withAnimation(.spring()) {
                                        self.authenticationViewModel.remembered.toggle()
                                    }
                                    
                                }) {
                                    HStack{
                                        Image(self.authenticationViewModel.remembered ? "checked" : "unchecked").renderingMode(.original)
                                            .resizable()
                                            .frame(width: 20, height: 20)
                                        Text("Remember me")
                                            .foregroundColor(Color.black.opacity(0.5))
                                            .font(Font.system(size: 15))
                                    }
                                }.padding(.leading, 5)
                                Spacer()
                                Button(action: {
                                    self.authenticationViewModel.forgotPassword()
                                }) {
                                    HStack{
                                        
                                        Text("Forgot password?")
                                            .foregroundColor(Color.black.opacity(0.5))
                                            .font(Font.system(size: 15))
                                    }
                                }
                                
                                
                            }.padding(.leading, 20)
                                .padding(.trailing, 20)
                            
                            Spacer()
                            
                        }
                            
                        .frame(width: UIScreen.main.bounds.size.width - 40, height: 300)
                        .background(Color.white)
                        .cornerRadius(25)
                        .shadow(color: Color.black.opacity(0.3)
                            , radius: 5, x: 5, y: 5)
                            .shadow(color: Color.black.opacity(0.2)
                                , radius: 5, x: -5, y: -5)
                    }
         
                    
                    ZStack{
                        ZStack{
                            //Login Action
                            Button(action: {
                                self.authenticationViewModel.login()
                            }) {
                                Image("forwardarrow")
                                    .resizable()
                                    .frame(width: 40, height: 40)
                            }
                            
                        }
                        .frame(width: 60, height: 60)
                        .foregroundColor(Color.white)
                        .background(LinearGradient(gradient: .init(colors: [Color(LoginSignUpColors.LoginSeconday ), Color(LoginSignUpColors.LoginPrimary )]), startPoint: .leading, endPoint: .trailing))
                            
                        .cornerRadius(30)
                        .shadow(color: Color.black.opacity(0.2), radius: 2, x: 2, y: 2)
                        .shadow(color: Color.black.opacity(0.1), radius: 2, x: -2, y: -2)
                    }.frame(width: 90, height: 90)
                        
                        .background(Color.white)
                        .cornerRadius(45)
                        
                        .offset( y: 190)
                    
                }
                .frame(width: UIScreen.main.bounds.size.width - 40, height: 350)
                Spacer()
                VStack(spacing: 40){
                    Text("Or Signin with")
                        .foregroundColor(Color.black.opacity(0.5))
                    HStack{
                        HStack{
                            Image("facebook")
                                .resizable()
                                .frame(width: 20, height: 20)
                            Text("Facebook")
                                .foregroundColor(Color.white)
                                .frame(width: 100)
                        }.frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 50)
                            .background(Color(LoginSignUpColors.HeaderColor))
                            .cornerRadius(25)
                            .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
                        
                        HStack{
                            Image("google")
                                .resizable()
                                .frame(width: 20, height: 20)
                            Text("Google")
                                .foregroundColor(Color.white)
                                .frame(width: 100)
                            
                        }.frame(width: UIScreen.main.bounds.size.width / 2 - 20, height: 50)
                            .background(Color(LoginSignUpColors.GoogleButtonBackground))
                            .cornerRadius(25)
                            .shadow(color: Color.black.opacity(0.2), radius: 4, x: 4, y: 4)
                    }.frame(width: UIScreen.main.bounds.size.width - 50)
                }.padding()
                    .padding(.bottom, 30)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            if !authenticationViewModel.isLoginSuccessFull{
                ErrorView(title: "Error", message: authenticationViewModel.error, isShown: $authenticationViewModel.isLoginSuccessFull)
            }
            if authenticationViewModel.resetPassword{
                ErrorView(title: "Error", message: authenticationViewModel.error, isShown: $authenticationViewModel.resetPassword)
            }
            if authenticationViewModel.isLoading{
                ActivityIndicator()
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        
    }
}

struct CustomTextField: View {
    @Binding var feildText: String
     var placeHolderText: String
    var imageName: String
    var isSecure: Bool
    var body: some View{
        HStack{
            Image(imageName)
            .resizable()
            .frame(width: 20, height: 20)
                .padding()
            if isSecure{
               SecureField(placeHolderText, text: $feildText)
            }else{
               TextField(placeHolderText, text: $feildText)
            }
            
        }.padding(.leading, 15)
            .padding(.trailing, 15)
            .frame(height: 55)
        .clipShape(Capsule())
        .modifier(TextFeildModifier())
    }
}

struct CsutomShape: Shape {
    var corner : UIRectCorner
    var radii : CGFloat
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corner, cornerRadii: CGSize(width: radii, height: radii))
        return Path(path.cgPath)
    }
}


struct TextFeildModifier: ViewModifier {
    func body(content: Content) -> some View {
        
        content.background(Color.white)
        .cornerRadius(25)
        .overlay(
        RoundedRectangle(cornerRadius: 25)
            .stroke(Color.black.opacity(0.05), lineWidth: 2)
            .shadow(color: Color.black.opacity(0.2), radius: 1, x: 0.4, y: 0.4)
            .clipShape(RoundedRectangle(cornerRadius: 25))
            .shadow(color: Color.black.opacity(0.2), radius: 5, x: -1, y: -1)
            .clipShape(RoundedRectangle(cornerRadius: 25))
        )
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

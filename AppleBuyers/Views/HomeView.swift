

import SwiftUI

struct HomeView: View {
    
    @ObservedObject var homeViewModel = HomeViewModel()
    //@ObservedObject var authViewModel:AuthenticationViewModel
    
    
    var body: some View {
        ZStack{
            
            //Header View
            VStack{
                HStack{
                    Button(action: {
                        self.homeViewModel.loadNotes()
                    }) {
                        Image("refresh").resizable()
                            .frame(width: 20, height: 20)
                            .padding(10)
                            .modifier(NeumorphicModifier(color: Color.white, radius: 15))
                        
                    }.padding(.leading, 20)
                    Spacer()
                    Text("Todo List").font(.largeTitle).bold()
                    Spacer()
                    Button(action: {
                        self.homeViewModel.logout()
                        UIApplication.shared.endEditing()
                        self.setRootView()
                    }) {
                        Image("logout").resizable()
                            .frame(width: 20, height: 20)
                            .padding(10)
                            .modifier(NeumorphicModifier(color: Color.white, radius: 15))
                        
                    }.padding(.trailing, 20)
                }.padding(.top, 20)
                Divider().shadow(color: Color.black.opacity(0.2), radius: 2, x: 2, y: 2)
                
                HomeSegmentView(viewModel: homeViewModel)
                // Check for Notes
                if self.homeViewModel.notes.isEmpty{
                   NoEntryAvailableView()
                }else{
                    List(self.homeViewModel.notes, id: \.title){ note in
                        HomeCell(noteModel: note)
                    }
                }
                Spacer()
            }.frame(maxWidth: .infinity, maxHeight: .infinity)
            
            //Adding Floating Action Button
            FloatingButtonView(homeViewModel: homeViewModel)
            
            //Adding Create Simple Note View
            CreateSimpleNoteView(isCreateSimpleNote: $homeViewModel.isCreateSimpleNote)
                .offset(y: homeViewModel.isCreateSimpleNote ? 0 : 2000)
            
            //Adding Create Simple Note View
            if homeViewModel.isCreateAppleBuilityNote{
                CreateAppleBilityView(isCreateAppleBuilityNote: $homeViewModel.isCreateAppleBuilityNote)
                               .offset(y: homeViewModel.isCreateAppleBuilityNote ? 0 : 2000)
            }
           
            
            // Add Error View
            if homeViewModel.isProcessCompleted{
                ErrorView(title: "Message", message: homeViewModel.message, isShown: $homeViewModel.isProcessCompleted)
            }
            
            // Add Loader View
            if homeViewModel.isLoading{
                ActivityIndicator()
            }
        }.frame(maxWidth: .infinity, maxHeight: .infinity)
        .onAppear{
             UITableView.appearance().separatorStyle = .none
            self.homeViewModel.loadNotes()
        }
        .navigationBarBackButtonHidden(true)
        .navigationBarHidden(true)
    }
    
    func setRootView(){
        let scene = UIApplication.shared.connectedScenes.first as? UIWindowScene
        
        if let widowSceneDelegate = scene?.delegate  as? SceneDelegate{
            let window = UIWindow(windowScene: scene!)
            
            window.rootViewController = UIHostingController(rootView: PhoneNumberLoginView())
            widowSceneDelegate.window = window
            window.makeKeyAndVisible()
        }
    }
}

// Add Cell view for list
struct HomeCell: View {
    var noteModel: NoteModel
    var body: some View{
        HStack{
            VStack(alignment: .leading){
                Text(noteModel.title).font(.headline).bold()
                Text(noteModel.detail).font(.subheadline).bold()
                    .padding(.top, 5)
                Text( "Perform date: \(noteModel.noteDateToPerform)").font(Font.system(size: 13))
                    .padding(.top, 5)
            }
            Spacer()
        }
        .frame(maxWidth: .infinity)
        .padding()
        .modifier(CustomTextFieldModifier())
    }
}


struct NeumorphicModifier: ViewModifier {
    var color: Color
    var radius: CGFloat
    func body(content: Content) -> some View {
        content.background(color)
            .cornerRadius(radius)
        .overlay(
            Circle()
                .stroke(Color.black.opacity(0.05), lineWidth: 4)
                .shadow(color: Color.black.opacity(0.2), radius: 3, x: 3, y: 3)
            .clipShape(Circle())
            .shadow(color: Color.black.opacity(0.2), radius: 3, x: -3, y: -3)
             .clipShape(Circle())
        )
    }
}

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
